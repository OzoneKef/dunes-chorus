console.log('App loaded');
let socket = {};
const state = { objects: [] };

window.onload = () => {
    state.canvas = document.getElementById('scene').getContext("2d");
    BootUp();
}

function BootUp(){
    const payload = state.control;
    Render();
    socket = io(`${location.hostname}:3001`);
    socket.on('connection', (sock) => {
        console.log('connected');
        sock.on('disconnect', (sock) => {
            console.log('disconnected');
        })
    })
}

function Render(){
    screenh = window.innerHeight;
    screenw = window.innerWidth;
    document.getElementById('scene').setAttribute('height', screenh);
    document.getElementById('scene').setAttribute('width', screenw);

    let screenfill = state.canvas.createRadialGradient(screenw/2, screenh/2, screenw/2, screenw/2, screenh/2, 0);
    screenfill.addColorStop(0, 'rgb(0, 0, 0)');
    screenfill.addColorStop(1, 'rgb(15, 15, 15)');

    state.canvas.fillStyle = screenfill;
    state.canvas.fillRect(0, 0, screenw, screenh);

    let tmpimg = null;
    state.objects.forEach((obj) => {
        tmpimg = new Image();
        tmpimg.src = `/res/${obj.sprite.icon}`;
        state.canvas.drawImage(
            tmpimg,
            0,
            0,
            obj.sprite.size.x,
            obj.sprite.size.y,
            obj.pos.x - state.eye.pos.x + screenw/2,
            obj.pos.y - state.eye.pos.y + screenh/2,
            obj.sprite.size.x,
            obj.sprite.size.y,         
        );
    })
}

function httpGETAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true);
}

function httpPOSTAsync(theUrl, payload, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("POST", theUrl, true);
    xmlHttp.setRequestHeader('Content-Type', 'application/json');
    xmlHttp.send(JSON.stringify(payload));
}