class Core{
    constructor(){
        this.modulesPaths = {
            ServerModule: './server/webserver',
            GameModule: './server/game.js',
            SocketModule: './server/socket',
        }

        this.modules = {};
    }

    Boot(){
        console.clear();
        let extender = null;
        for (let name in this.modulesPaths) {
            extender = require(this.modulesPaths[name]);
            this.modules[name] = new extender();
            extender = null;
        }
        console.log('Boot complete');
    }

    async Reboot(){
        for (let module in this.modules){
            await this.modules.Kill();
            delete this.modules[module];
        }
        this.Boot();
    }

    async RebootModule(name){
        await this.modules[name].Kill();
        delete this.modules[name];
        let extender = require(this.modulesPaths[name]);
        this.modules[name] = new extender();
        extender = null;
    }
}

global.Core = new Core();
global.Core.Boot();

global.Core.modules.GameModule.Default();