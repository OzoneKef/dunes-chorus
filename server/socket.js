const socketio = require('socket.io');
const Module = require('./module');


class Socket extends Module{
    constructor(...args){
        super(...args);
        this.Init();
    }

    Init(){
        this.io = socketio(3001);
        this.io.on('connection', function(socket){
            console.log('a user connected');
        });
    }
}

module.exports = Socket;
