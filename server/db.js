const mysql = require('mysql');
const OPTIONS = require('../config');

class DB {
    makeQuery(query, args){
        return new Promise(async (resolve, reject) => {
            const connection = await mysql.createConnection(OPTIONS.DB);
            await connection.connect();
            connection.query(query, args, (error, results, fields) => {
                connection.end();
                if (!error){
                    resolve(results);
                } else {
                    reject(error);
                }
            });
        })
    }
}

const DBresolver = new DB;

module.exports = DBresolver;