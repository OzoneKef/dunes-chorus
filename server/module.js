const DB = require('./db');

class Module{
    constructor(){
        
    }

    async makeQuery(query, args){
        return (await DB.makeQuery(query, args));
    }

    Kill(){
        return {success: 1};
    }
}

module.exports = Module;