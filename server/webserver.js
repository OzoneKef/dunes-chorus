const Module = require('./module');
const Express = require('express')();
const path = require('path');
const md5 = require('md5');

class WebServer extends Module{
    constructor(...args){
        super(...args);
        this.Init();
    }

    Init(){
        Express.use(require('body-parser').urlencoded({ extended: false }));
        Express.use(require('body-parser').json({ type: 'application/json' }));
        Express.use(require('cookie-parser')());

        Express.post('/API/user/register', async (req, res) => {
            const { username, password } = req.body;
            const oldaccount = (await this.makeQuery('SELECT id FROM users WHERE name=?', [username]))[0];
            if (oldaccount) {
                res.json({success: 0, error: 'Username already taken'});
            } else if (password.length < 6) {
                res.json({success: 0, error: 'Password too short'});
            } else {
                await this.makeQuery('INSERT INTO users (name, pass, registered) VALUES (?, ?, ?)', [username, md5(password), +new Date()]);
                res.json({success: 1, username, sessionkey: await this.LoginUser(username, password)});
            }
        })

        Express.post('/API/user/login', async (req, res) => {
            const {username, password } = req.body;
            res.json({success: 1, username, sessionkey: await this.LoginUser(username, password)})
        })

        Express.get('/API/user/logout', async (req, res) => {
            const { sessionkey } = req.cookies;
            if (sessionkey){
                await this.makeQuery('UPDATE sessions SET closed=1 WHERE `key`=?', [sessionkey]);
                res.json({success: 1, message: 'pokedova'})
            } else {
                res.json({success: 0, error: 'No active sessions on this machine'})
            }
        })

        Express.get('/res/:src', async (req, res) => {
            const { src } = req.params;
            res.sendFile(path.resolve(`./client/resources/${src}`));
        }) 

        Express.get('/appcore', async (req, res) => {
            if (!req.cookies.sessionkey || (await this.CheckSession(req.cookies.sessionkey))){
                res.json({success: 0, error: 'You are not authorized to get app'});
            } else {
                res.sendFile(path.resolve(`./client/app.js`));
            }
        })

        Express.get('/style', async (req, res) => {
            res.sendFile(path.resolve(`./client/style.css`));
        })

        Express.get('/state', async (req, res) => {
            const { username } = req.cookies;
            const player = global.Core.modules.GameModule.players[username];
            try{
                res.json({observable: player.observable, eye: player.eye});
            }catch (error){
                res.json({success: 0, error});
            }
        })

        Express.get('/', async (req, res) => {
            if (!req.cookies.sessionkey || (await this.CheckSession(req.cookies.sessionkey))){
                res.sendFile(path.resolve(`./client/login.html`));
            } else {
                res.sendFile(path.resolve(`./client/app.html`));
            }
        })


        Express.listen(3000, () => {
            console.log('Now listen on port 3000')
        })
    }

    async CheckSession(sessionkey){
        if ((await this.makeQuery('SELECT id FROM sessions WHERE `key`=? AND closed=0', [sessionkey]))[0]){
            return false;
        } else {
            return true;
        }
    }

    async LoginUser(username, password){
        let userid = await this.makeQuery('SELECT id FROM users WHERE name=? AND pass=?', [username, md5(password)]);
        let sessionkey = null;
        if (userid[0]) {
            await this.makeQuery('UPDATE sessions SET closed=1 WHERE userid=?', [userid[0].id]);
            sessionkey = generateSessionKey(username, 32);
            await this.makeQuery('INSERT INTO sessions (`key`, userid, closed) VALUES (?, ?, 0)', [sessionkey, userid[0].id]);
        }
        global.Core.modules.GameModule.SpawnPlayer(username);
        return sessionkey;
    }
}

module.exports = WebServer;

const symbolarr = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
function generateSessionKey(name, length){
    let key = name;
    for (let index=0; index < length; index++){
        key += symbolarr[Math.floor(Math.random()*symbolarr.length)];
    }
    return key;
}