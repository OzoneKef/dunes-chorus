class Object{
    constructor({ name, pos }){
        this.name = name || 'Object';
        this.pos = pos || {x: 0, y: 0};
        this.phys = {
            mass: 0,
            speed: {
                x: 0, y: 0,
            },
            dir: 0,
            volume: {x: 0, y: 0, z: 0},
            gcoeff: 0,
            temperature: 0,
        }
        this.sprite = {
            icon: 'object.png',
            size: {x: 32, y: 32},
        };
        global.Core.modules.GameModule.objects.push(this);
    }

    async Simulate(){
        await this.accelerate();
        await this.move();
        await this.warm();
    }

    async accelerate(){

    }

    async move(){
        this.pos = {
            x: this.pos.x + this.phys.speed.x,
            y: this.pos.y + this.phys.speed.y,
        }
        this.phys.dir = this.phys.dir += this.phys.ang;
    }

    async warm(){

    }
}

module.exports = { def: Object };

const Test = require('./obj/test');

module.exports = { def: Object, test: Test };