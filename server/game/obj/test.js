const ObjTree = require('../object');

class Test extends ObjTree.def{
    constructor(...args){
        super(...args)
        this.material = {
            color: {
                r: Math.floor(Math.random()*255),
                g: Math.floor(Math.random()*255),
                b: Math.floor(Math.random()*255),
            }
        }
    }
}

module.exports = Test;