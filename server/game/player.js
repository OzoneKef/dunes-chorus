class Player{
    constructor(object){
        this.eye = object;
        this.farsight = 500;
        this.observable = [];
    }

    async Simulate(){
        this.observable = [];
        global.Core.modules.GameModule.objects.forEach((object) => {
            if ((Math.abs(this.eye.pos.x - object.pos.x) < this.farsight) && (Math.abs(this.eye.pos.y - object.pos.y) < this.farsight)) {
                this.observable.push(object);
            }
        })
    }

    async Control(control){
        let x = 0;
        let y = 0;
        if (control.hold.s) { y += 0.5 };
        if (control.hold.w) { y -= 0.5 };
        if (control.hold.d) { x += 0.5 };
        if (control.hold.a) { x -= 0.5 };
        this.eye.phys.speed = { x, y }
    }
}

module.exports = Player;