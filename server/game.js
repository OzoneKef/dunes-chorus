const Module = require('./module');
const ObjTree = require('./game/object');
const Player = require('./game/player');

class Game extends Module{
    constructor(...args){
        super(...args);
        this.objects = [];
        this.players = {};
        this.Init();
    }

    async Init(){
        setInterval(() => {
            try {
                this.Simulate();
            } catch (error) {
                console.log(`It's dead, Jim`);
            }
        }, 1000/60);
    }

    async SpawnPlayer(username){
        const Mob = new ObjTree.test(username);
        this.players[username] = new Player(Mob);
    }

    async Simulate(){
        this.objects.forEach((obj) => {
            obj.Simulate();
        })
        for (let player in this.players){
            this.players[player].Simulate();
        }
    }

    Default(){
        new ObjTree.def({pos:{x:100, y: 100}});
        new ObjTree.def({pos:{x:-100, y: 100}});
        new ObjTree.def({pos:{x:100, y: -100}});
        new ObjTree.def({pos:{x:600, y: -100}});
    }
}

module.exports = Game;